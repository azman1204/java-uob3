package module1;

public class StaticDemo {
    // this belongs to class
    public static final String NAME = "John Doe";


    // this belongs to object
    public int age = 30;

    public static void main(String[] args) {
        System.out.println(StaticDemo.NAME);
        //NAME = "sss"; // cannot modify the value of constant / final var
        StaticDemo demo = new StaticDemo(); // instantiate an object
        System.out.println(demo.age);
    }
}
