package module1;

public class Conditional {
    public static void main(String[] args) {
        int age = 10;
        // float, double, char, boolean, short, long, int, byte - native type
        // String , Car, Person, ..
        String level = "";
        if (age > 7 && age < 18) {
            level = "teenager";
        } else if (age >= 18 && age < 50) {
            level = "adult";
        } else if (age >= 50) {
            level = "old";
        }
        System.out.println("Level = " + level);

        String name = "x";
        switch(name) {
            case "M":
                System.out.println("Mother");
                break;
            case "F":
                System.out.println("Father");
                break;
            default:
                System.out.println("Others");
        }

        // ternary conditional
//        if (age > 18) {
//            return "Adult";
//        } else {
//            return "Children";
//        }
        //(condition) ? do this if true : else do this
        String level2 = age > 18 ? "Adult" : "Children";
        System.out.println("Level 2 = " + level2);

        String firstName = "John";
        String lastName = "Jane";

        // this seem ok - memory location comparison - don't use this method
        if (firstName == lastName) {
            System.out.println("names are equal");
        } else {
            System.out.println("Names not equal");
        }

        // normally other than primitive data we use .equals() - deep comparison
        if (firstName.equals(lastName)) {
            System.out.println("first and last equal");
        } else {
            System.out.println("First and last not equal");
        }
    }
}
