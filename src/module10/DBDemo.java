package module10;

import java.sql.*;

public class DBDemo {
    public static void main(String[] args) {
        new DBDemo().doConnect();
    }

    public void doConnect() {
        String url = "jdbc:mysql://localhost/javatot";
        try {
            Connection conn = DriverManager.getConnection(url, "root", "azman1204");
            System.out.println("connection ok");
            Statement stmt = conn.createStatement();

            // insert
            String sql = "INSERT INTO person(name, age, salary)" +
                         "VALUES('Jane Doe', 35, 15000)";
            stmt.execute(sql);

            sql = "SELECT * FROM person";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()) {
                System.out.println(rs.getString("name"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
