package module3;

import java.util.HashSet;

public class HashsetDemo {
    public static void main(String[] args) {
        HashSet<String> names = new HashSet<>();
        names.add("John Doe");
        names.add("Abu");
        names.add("Ravi");
        names.add("Ravi");
        for(String name: names) {
            System.out.println(name);
        }
    }

    // ArrayList vs HashSet
    // 1. arrayList - ordered
    // 2. Hashset cannot have duplicate value
}
