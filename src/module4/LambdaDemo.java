package module4;

public class LambdaDemo {
    public static void main(String[] args) {
//        Message msg = () -> {
//            System.out.println("Hello Lambda");
//        };

        Message msg = () -> System.out.println("Hello Lambda");

        msg.message();

        Alert alert = (str) -> {
            String s = "You are in " + str;
            return s;
        };

        String str = alert.alert("Danger !");
        System.out.println(str);
    }
}

// @FunctionalInterface is optional
interface Message {
    public void message();
    //public void hello();
}

@FunctionalInterface
interface Alert {
    public String alert(String str);
}

// rule of lambda
// 1. must come with interface
// 2. the interface must have only one method declaration