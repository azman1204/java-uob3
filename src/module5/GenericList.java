package module5;

// T = type
public class GenericList<T> {
    private T[] items = (T[]) new Object[10];
    private int count;

    public void add(T item) {
        items[count++] = item;
    }

    public T get(int index) {
        return items[index];
    }

    public static void main(String[] args) {
        var list = new GenericList<Integer>();
        list.add(250);
        list.add(350);
        System.out.println(list.get(1));

        var list2 = new GenericList<String>();
        list2.add("John Doe");
        list2.add("Jane Doe");
        //list2.add(100);
        System.out.println(list2.get(1));
    }

    // summary.. why generic
    // 1. to summarize similar class into one class
    // 2. prevent error of typing in array
}

