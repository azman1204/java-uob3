package module5;

public class Mylist2 {
    private Object[] items = new Object[10];
    private int count = 0;

    public void add(Object item) {
        items[count++] = item;
    }

    public Object get(int index) {
        return items[index];
    }

    public static void main(String[] args) {
        var list = new Mylist2();
        list.add(new String("Azman"));
        list.add(200);
        int no = (int) list.get(0);
        System.out.println("No = " + no);
        System.out.println(list.get(0));
    }
}
