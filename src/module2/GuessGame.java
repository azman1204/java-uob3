package module2;

import java.util.Scanner;

public class GuessGame {
    public static void main(String[] args) {
        new GuessGame().play();
    }

    public void play() {
        // Math.random 0 .. 1
        int randNo = (int) (Math.random() * 100);
        Scanner scanner = new Scanner(System.in);
        int noTrial = 0;
        while(true) {
            System.out.print("Guess : ");
            int guessNo = scanner.nextInt();
            if (guessNo == randNo) {
                System.out.println("Hooray.. success after " + noTrial);
                break;
            } else {
                if (guessNo > randNo) {
                    System.out.println("Guess lower");
                } else {
                    System.out.println("Guess higher");
                }
            }
            noTrial++;
        }
    }
}
