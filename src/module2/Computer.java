package module2;
public interface Computer {
    public String name = "test"; // no need to override
    public String getDeviceType();
    public String getSpeed();
}


