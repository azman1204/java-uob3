package module2;

import java.util.Scanner;

public class ObjectDemo2 {
    public static void main(String[] args) {
        // instantiate an obj
        ObjectDemo demo = new ObjectDemo("HP");
        ObjectDemo demo2 = new ObjectDemo(3.5, "Lenovo");
        System.out.println(demo2);

        int no = (int) (Math.random() * 100);
        System.out.println(no);

        Scanner scanner = new Scanner(System.in);
        int no2 = scanner.nextInt(); // read input as int
    }
}
