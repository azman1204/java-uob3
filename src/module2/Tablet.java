package module2;

// one class can implement more than one interface
public class Tablet implements Computer {

    @Override
    public String getDeviceType() {
        return "it's a tablet";
    }

    @Override
    public String getSpeed() {
        return "3.5Ghz";
    }

    public static void main(String[] args) {
        Tablet tablet = new Tablet();
        System.out.println(tablet.getSpeed() + tablet.name);
    }
}
