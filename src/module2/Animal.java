package module2;

// access modifier - public , protected , private
public class Animal {
    String type; // mamal, reptilian, fish..
    String species;

    @Override
    public String toString() {
        return "Type = " + type + " species = " + species;
    }
}

class Monkey extends Animal {
    // overriding
    public String toString() {
        return "Monkey Type = " + type + " Monkey species = " + species;
    }
}

class Demo {
    public static void main(String[] args) {
        Monkey monkey = new Monkey();
        monkey.type = "mamal";
        monkey.species = "Orang Hutan";
        System.out.println(monkey);

        // monkey is animal
        Animal monkey2 = new Monkey();
        monkey2.type = "mamal";
        monkey2.species = "Kera";
        System.out.println(monkey2);

        Bird bird = new Bird();
        bird.setName("Dove");
        System.out.println("Bird name = " + bird.getName());
    }
}