package module2;

public class ObjectDemo {
    // fields / properties
    double cpuSpeed = 2.5;
    String brand;

    // constructor / method - initialize fields
    // naming rule - same name to class
    // overloading
    ObjectDemo(String brand) {
        brand = "Dell";
        //this.brand = "Dell";
    }

    ObjectDemo(double cpuSpeed, String brand) {
        this.cpuSpeed = cpuSpeed;
        this.brand = brand;
    }

    public String toString() {
        return "brand = " + brand +
                "\n cpu speed = " + cpuSpeed;
    }
}
