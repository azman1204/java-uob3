package module9;

// Enumeration - store a list of predefined set of values
// i.e SUN, MON, TUE ... SAT (constant)

enum Days {
    SUN, MON, TUE, WED, THU, FRI, SAT
};

// why enum:
// 1. prevent error
// 2. make your code easy to read
// 3. to store and re-use predefined values

public class EnumDemo {
    public static void main(String[] args) {
        Days day = Days.SUN;
        switch(day) {
            case SUN:
                System.out.println("Today is sunday");
                break;
            case MON:
                System.out.println("Today is sunday");
                break;
            default:
                System.out.println("i dont know");
        }
    }
}
