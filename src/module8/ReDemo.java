package module8;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReDemo {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("house", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher("visit my school");
        boolean matchFound = matcher.find();
        if (matchFound)
            System.out.println("Match found");
        else
            System.out.println("Not match");

        ReDemo.icValidation();
        ReDemo.emailValidation();
        ReDemo.stringSplitter();
    }

    public static void icValidation() {
        // 123456-10-1234
        // [0-9]{6}-[0-9]{2}-[0-9]{4}
//        Pattern pattern = Pattern.compile("[0-9]{6}-[0-9]{2}-[0-9]{4}");
        Pattern pattern = Pattern.compile("[0-9]{6}-10-[0-9]{4}");
        Matcher matcher = pattern.matcher("123456-10-1234");
        boolean matchFound = matcher.find();
        if (matchFound)
            System.out.println("Match");
        else
            System.out.println("Not match");
    }

    public static void emailValidation() {
        Pattern pattern = Pattern.compile("[a-zA-Z0-9]{1,}@[a-zA-Z0-9]{1,}(.[a-zA-Z]{2,3})+");
        Matcher matcher = pattern.matcher("12azman@gmail.com.my");
        boolean matchFound = matcher.find();
        if (matchFound)
            System.out.println("Match");
        else
            System.out.println("Not match");
    }

    public static void stringSplitter() {
        Pattern pattern = Pattern.compile("for");
        String str = "Another Opposition for declares support for Anwar's administration, but remains loyal to Bersatu";
        String[] arr = pattern.split(str);
        //System.out.println(arr);
        for(String s: arr) {
            System.out.println(s);
        }
    }
}
