package module6;

public class StringDemo {
    public static void main(String[] args) {
        String name = "aZMan ZAKAria";
        System.out.println(name.toLowerCase());
        System.out.println(name.toUpperCase());
        String name2 = "John Doe";
        System.out.println(name2.replace('o', 'a'));
        System.out.println(name2);

        String str = "This is java SE Training at Trainocate";
        if (str.contains("UOB")) {
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }

        System.out.println(str.replace("java", "Python"));

        // substring "This is java"
        int index = str.indexOf("java") + "java".length();
        int index2 = str.lastIndexOf("java");
        System.out.println("index = " + index + " last index = " + index2);
        //System.out.println(str.substring(0, 12));
        System.out.println(str.substring(0, index));
    }
}
